﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MyMesh
{
    public static MyMesh Combine(params MyMesh[] ms)
    {
        MyMesh m = ms[0].Copy();
        for (int i = 1; i < ms.Length; i++)
        {
            m = Combine(m, ms[i]);
        }
        return m;
    }
    public static MyMesh Combine(MyMesh m1, MyMesh m2)
    {
        MyMesh m = m1.Copy();

        if (m2.weldable)
        {
            int triCnt = 0;
            int[] tri;
            Vector3[] verts;
            int eqCnt = 0;
            int chIndex0 = -1, chIndex1 = -1;
            while (triCnt != m2.TrisCount)
            {
                // getting m2 triangle
                tri = m2.GetTriInds(triCnt);
                verts = m2.GetTriVerts(triCnt);

                eqCnt = 0;
                chIndex0 = -1; chIndex1 = -1;
                for (int i = 0; i < m.verts.Length; i++)
                {
                    for (int j = 0; j < verts.Length; j++)
                    {
                        if (m.verts[i] == verts[j])
                        {
                            eqCnt++;
                            tri[j] = i;
                            if (chIndex0 == -1)
                                chIndex0 = j;
                            else if (chIndex1 == -1)
                                chIndex1 = j;
                        }
                    }
                }

                // Если кол-во совпавших вершин 2, то индексы переносимого треугольника меняются на соответствующие в двух местах, а 
                // третий индекс(не измененный) становится равный макс. индекс первой меши + 1
                if (eqCnt == 2)
                {
                    for (int i = 0; i < tri.Length; i++)
                        if (i != chIndex0 && i != chIndex1)
                        {
                            //ArrayUtility.Add<Vector3>(ref m.verts, verts[tri[i]]);
                            ArrayUtility.Add<Vector3>(ref m.verts, m2.verts[tri[i]]);
                            tri[i] = m.MaxIndex + 1;
                        }
                }
                else if (eqCnt == 1)
                {
                    int iter = 0;
                    for (int i = 0; i < tri.Length; i++)
                        if (i != chIndex0)
                        {
                            ArrayUtility.Add<Vector3>(ref m.verts, m2.verts[tri[i]]);
                            iter++;
                            tri[i] = m.MaxIndex + iter;
                        }
                }
                else if (eqCnt == 0)
                {
                    ArrayUtility.AddRange(ref m.verts, /*verts*/m2.verts);
                    for (int i = 0; i < tri.Length; i++)
                        tri[i] = m.MaxIndex + 1 + i; // may be error
                }
                ArrayUtility.AddRange(ref m.inds, tri);

                triCnt++;
            }
        }
        else
        {
            ArrayUtility.AddRange(ref m.verts, m2.verts);
            int[] tris = new int[m2.inds.Length];
            for (int i = 0; i < tris.Length; i++)
                tris[i] = m2.inds[i] + m.MaxIndex + 1;
            ArrayUtility.AddRange(ref m.inds, tris);
        }

        return m;
    }

    public static MyMesh Quad(float w, float h)
    {
        MyMesh m = new MyMesh(new Mesh(), new Vector3[] { new Vector3(0, 0), new Vector3(0, h), new Vector3(w, h), new Vector3(w, 0) },
            new int[] { 0, 1, 2, 2, 3, 0 });
        return m;
    }

    Mesh main;
    Vector3[] verts;
    public Vector3[] Verts { get { return verts; } }
    int[] inds;
    public bool weldable = false;

    public int MaxIndex { get { return Mathf.Max(inds); } }
    public int MinIndex { get { return Mathf.Min(inds); } }
    public int TrisCount { get { return inds.Length / 3; } }

    public int[] GetTriInds(int I)
    {
        int[] tri = new int[3];

        tri[0] = inds[I * 3 + 0];
        tri[1] = inds[I * 3 + 1];
        tri[2] = inds[I * 3 + 2];

        return tri;
    }
    public Vector3[] GetTriVerts(int I)
    {
        Vector3[] vs = new Vector3[3];
        int[] tri = GetTriInds(I);
        vs[0] = verts[tri[0]];
        vs[1] = verts[tri[1]];
        vs[2] = verts[tri[2]];

        return vs;
    }

    public void InvertTri(int I)
    {
        I *= 3;
        int swp = inds[I + 0];
        inds[I + 0] = inds[I + 2];
        inds[I + 2] = swp;
    }
    public void RemoveTri(int I)
    {
        I *= 3;
        ArrayUtility.RemoveAt(ref inds, I);
        ArrayUtility.RemoveAt(ref inds, I);
        ArrayUtility.RemoveAt(ref inds, I);
    }

    public int InsertVertex(Vector3 vertex, int triIndex)
    {
        int[] tri = GetTriInds(triIndex);
        ArrayUtility.Add(ref verts, vertex);

        RemoveTri(triIndex);

        int[] newTri = new int[3 * 3];
        newTri[0] = tri[0]; newTri[1] = tri[1]; newTri[2] = verts.Length - 1;
        newTri[3] = tri[1]; newTri[4] = tri[2]; newTri[5] = verts.Length - 1;
        newTri[6] = tri[2]; newTri[7] = tri[0]; newTri[8] = verts.Length - 1;

        ArrayUtility.AddRange(ref inds, newTri);

        return verts.Length - 1;
    }

    public Vector3 Size
    {
        get
        {
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue),
                min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            for (int i = 0; i < verts.Length; i++)
            {
                if (verts[i].x > max.x) max.x = verts[i].x;
                if (verts[i].y > max.y) max.y = verts[i].y;
                if (verts[i].z > max.z) max.z = verts[i].z;

                if (verts[i].x < min.x) min.x = verts[i].x;
                if (verts[i].y < min.y) min.y = verts[i].y;
                if (verts[i].z < min.z) min.z = verts[i].z;
            }

            return max - min;
        }
    }

    public MyMesh(Mesh Main)
    {
        main = Main; FromMesh(Main);
    }
    public MyMesh(Mesh Main, params Vector3[] Verts)
    {
        main = Main;
        FromMesh(Main);
        verts = Verts;
        inds = new int[verts.Length];
        for (int i = 0; i < verts.Length; i++) inds[i] = i;
    }
    public MyMesh(Mesh Main, Vector3[] Verts, int[] Inds)
    {
        main = Main;
        FromMesh(Main);
        this.inds = Inds;
        this.verts = Verts;
    }

    public MyMesh Copy()
    {
        MyMesh m = new MyMesh(new Mesh());

        m.verts = new Vector3[verts.Length];
        for (int i = 0; i < verts.Length; i++)
            m.verts[i] = verts[i];

        m.inds = new int[inds.Length];
        for (int i = 0; i < inds.Length; i++)
            m.inds[i] = inds[i];

        m.weldable = weldable;

        return m;
    }
    public Mesh ToMesh()
    {
        main.vertices = verts;
        main.triangles = inds;
        return main;
    }
    public void FromMesh(Mesh m)
    {
        verts = m.vertices;
        inds = m.triangles;
    }
}
