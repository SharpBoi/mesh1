﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MeshInfo : MonoBehaviour
{
    [HideInInspector]
    public MeshFilter mf = null;

    [HideInInspector]
    public bool gizmoVerts = false;

	void Start ()
    {
		
	}
	void Update ()
    {
		
	}

    private void OnDrawGizmos()
    {
        if (gizmoVerts)
        {
            Gizmos.color = Color.red;
            for (int i = 0; i < mf.sharedMesh.vertices.Length; i++)
                Gizmos.DrawSphere(transform.TransformPoint(mf.sharedMesh.vertices[i]), 0.05f);
            Gizmos.color = Color.white;

        }
    }
}

[CustomEditor(typeof(MeshInfo))]
class MeshInfoInsp : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var tgt = target as MeshInfo;

        if (tgt.mf == null)
            tgt.mf = tgt.GetComponent<MeshFilter>();

        GUILayout.Label("Tris count: " + tgt.mf.sharedMesh.triangles.Length / 3);
        GUILayout.Label("Verts count: " + tgt.mf.sharedMesh.vertices.Length);
        GUILayout.Label("Normals count: " + tgt.mf.sharedMesh.normals.Length);

        tgt.gizmoVerts = GUILayout.Toggle(tgt.gizmoVerts, "Gizmo verts");
    }
}