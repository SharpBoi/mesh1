﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Vector3 curScreen;
    Vector3 difCurScreen;
    float difZRot = 0f;
    float zRot = 0f;
    public float MouseSense = 0.5f;

    Rigidbody rigidBody;

    public float Braking = 0.8f;
    public float MaxVel = 1.5f;
    [HideInInspector]
    public float CurrentVel = 0f;
    Vector3 vel;
    bool keyPressed = false;

	void Start ()
    {
        rigidBody = GetComponent<Rigidbody>();
	}

    void FixedUpdate()
    {
        difCurScreen = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        curScreen += difCurScreen;

        // ROTATING
        if (Input.GetKey(KeyCode.Q))
            zRot += (difZRot = 1);
        if (Input.GetKey(KeyCode.E))
            zRot += (difZRot = -1);
        rigidBody.transform.Rotate(-difCurScreen.y * MouseSense, difCurScreen.x * MouseSense, difZRot * MouseSense);
        difZRot = 0;

        // MOVING
        CurrentVel = 0;
        keyPressed = false;
        #region Keys
        if (Input.GetKey(KeyCode.W))
        {
            keyPressed = true;
            CurrentVel = MaxVel;
            rigidBody.AddForce(rigidBody.transform.forward * CurrentVel, ForceMode.Force);
        }
        if (Input.GetKey(KeyCode.A))
        {
            keyPressed = true;
            CurrentVel = MaxVel;
            rigidBody.AddForce(-rigidBody.transform.right * CurrentVel, ForceMode.Force);
        }
        if (Input.GetKey(KeyCode.S))
        {
            keyPressed = true;
            CurrentVel = MaxVel;
            rigidBody.AddForce(-rigidBody.transform.forward * CurrentVel, ForceMode.Force);
        }
        if (Input.GetKey(KeyCode.D))
        {
            keyPressed = true;
            CurrentVel = MaxVel;
            rigidBody.AddForce(rigidBody.transform.right * CurrentVel, ForceMode.Force);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            keyPressed = true;
            CurrentVel = MaxVel;
            rigidBody.AddForce(rigidBody.transform.up * CurrentVel, ForceMode.Force);
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            keyPressed = true;
            CurrentVel = MaxVel;
            rigidBody.AddForce(-rigidBody.transform.up * CurrentVel, ForceMode.Force);
        }
        if (!keyPressed)
            rigidBody.velocity *= Braking;
        #endregion


    }

    void OnDrawGizmos()
    {

    }
}
