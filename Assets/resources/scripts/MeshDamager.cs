﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Создает эффекты повреждения на меши
/// </summary>
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
public class MeshDamager : MonoBehaviour
{
    MeshFilter mf = null;
    MeshCollider mc = null;

    [HideInInspector]
    public MyMesh mesh;
    [HideInInspector]
    public MyMesh beginMesh;

    public ComputeShader shader;
    public Texture2D result;

	void Start ()
    {
        mf = GetComponent<MeshFilter>();
        mc = GetComponent<MeshCollider>();

        mesh = new MyMesh(mf.sharedMesh);
        beginMesh = new MyMesh(mf.sharedMesh);
	}
    public void UpdateMesh()
    {
        if (mf == null) mf = GetComponent<MeshFilter>();
        if (mc == null) mc = GetComponent<MeshCollider>();
        mf.sharedMesh = mesh.ToMesh();
        mf.sharedMesh.RecalculateNormals();
        mc.sharedMesh = mf.sharedMesh;
    }

    public void MakeDent(Vector3 p, float r)
    {
        int ki = shader.FindKernel("CSMain");

        Texture2D t = new Texture2D(512, 512, TextureFormat.RGB24, false, false);
        t.Apply();

        result = t;

        //shader.SetTexture(ki, "Result", rt);
        shader.SetTexture(ki, "REsult", t);
        shader.Dispatch(ki, t.width / 8, t.height / 8, 1);

        result = t;
    }
}

[CustomEditor(typeof(MeshDamager))]
class MeshProcessorInsp : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var tgt = target as MeshDamager;
    }
}
