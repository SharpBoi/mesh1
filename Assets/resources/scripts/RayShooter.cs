﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayShooter : MonoBehaviour
{
    Ray ray = new Ray();
    RaycastHit hit = new RaycastHit();

	void Start ()
    {
        
	}
	
	void Update ()
    {
        if (hit.collider != null)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                var mp = hit.collider.GetComponent<MeshDamager>();

                int ai = mp.mesh.InsertVertex(hit.transform.InverseTransformPoint(hit.point), hit.triangleIndex);
                mp.mesh.Verts[ai] += hit.normal /** 0.1f*/;
                mp.UpdateMesh();

                //mp.MakeDent(new Vector3(), 0.03f);
            }
        }
    }

    private void FixedUpdate()
    {
        ray.origin = Camera.main.transform.position;
        ray.direction = Camera.main.transform.forward;

        Physics.Raycast(ray, out hit);
    }

    private void OnDrawGizmos()
    {
        if (hit.collider != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(hit.point, 0.02f);
        }
    }
}
